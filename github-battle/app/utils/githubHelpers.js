import axios from 'axios';

var id = "YOUR_CLIENT_ID";
var sec = "YOUR_SECRET_ID";
var params = `?client_id=${id}&client_secret=${sec}`;

function getUserInfo(username){
    return axios.get(`https://api.github.com/users/${username}${params}`);
}

function getRepos(username){
    // get users repos
    return axios.get(`https://api.github.com/users/${username}/repos${params}&per_page=100`);
}

function getTotalStars(repos){
    // calculate all of the total stars the user has
    return repos.data.reduce(function(prev,curr){
        return prev + curr.stargazers_count
    }, 0);
}

function getPlayersData(player){
    // get repos
    // pass info to total stars
    // return obj with data
    return getRepos(player.login)
        .then(getTotalStars)
        .then(function (totalStars){
            return {
                followers: player.followers,
                totalStars: totalStars
            }
        });
}

function calculateScores (players){
    // return an array, after doing algorithims to determine winner
    return [
        players[0].followers * 3 + players[0].totalStars,
        players[1].followers * 3 + players[1].totalStars
    ]
}

var helpers = {
    getPlayersInfo: function(players){
        return axios.all(players.map(function(username){
            return getUserInfo(username);
        })).then(function(response){
            return response.map(function(resObj){
                return resObj.data;
            });
        }).catch(function(err){
            console.warn('Error in getPLayersInfo', err);
        });
    },
    battle: function(players){
        var playerOneData = getPlayersData(players[0]);
        var playerTwoData = getPlayersData(players[1]);
        return axios.all([playerOneData,playerTwoData])
        .then(calculateScores)
        .catch(function(err){console.error('Error in getting player info', err)});
    }
};

module.exports = helpers;
