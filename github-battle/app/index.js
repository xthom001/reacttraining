import React from 'react';
import Raven from 'raven-js';
var ReactDOM = require('react-dom');
var routes = require('./config/route');

var sentryKey = '917ff759ce9c40beb23508082c605115';
var sentryApp = '103482';
var sentryUrl = `https://${sentryKey}@sentry.io/${sentryApp}`;

var _APP_INFO = {
    name: 'Github Battle',
    branch: 'master',
    version: '1.0'
};

Raven.config(sentryUrl,{
    release: _APP_INFO.version,
    tags:{
        branch: _APP_INFO.branch
    }
}).install();

//Second Lesson

ReactDOM.render(
    routes,
    document.getElementById('app')
)
