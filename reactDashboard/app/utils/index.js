import Moment from 'moment';
import md5 from 'md5';

export default {
    formatTime: (time) =>{
        return Moment(time).format('MMM Do YY, h:mm a');
    },
    hashCode: (val) => {
        return md5(val);
    },
    makeCancelable: (promise) => {
        let hasCanceled_ = false;

        const wrappedPromise = new Promise((resolve, reject) => {
            promise.then((val) =>
                hasCanceled_ ? reject({isCanceled: true}) : resolve(val)
            );
            promise.catch((err) =>
                hasCanceled_ ? reject({isCanceled: true}) : reject(error)
            );
        });

        return {
            promise: wrappedPromise,
            cancel(){
                hasCanceled_ = true;
            }
        }
    }
}
