import React from 'react';
import {render} from 'react-dom';
import Raven from 'raven-js';
import routes from './config/route';


var sentryKey = 'f15e73e659484126a6a8984d3a1a0c50';
var sentryApp = '113941';
var sentryUrl = `https://${sentryKey}@sentry.io/${sentryApp}`;

var _APP_INFO = {
    name: 'React Dash',
    branch: 'master',
    version: '1.0'
};

Raven.config(sentryUrl,{
    release: _APP_INFO.version,
    tags:{
        branch: _APP_INFO.branch
    }
}).install();

render(routes, document.getElementById('app'))
