import React, {Component} from 'react';
import Github from 'github-api';
import {ButtonGroup, Button} from 'react-bootstrap';
import {ProjectList,GithubModal} from './components';

class Projects extends Component {
    constructor(props) {
        super(props);
        this.init();
    }



    init() {
        var wrapperObj = {
            auth: {},
            isActive: false,
            showModal: false,
            repoData: [],
            error: null
        }

        this.state = {
            projectList: this.props.currentUser.projectList.map((projects)=>{
                Object.assign(projects,wrapperObj);
                return projects;
            })
        }

        this.toggleProject = this.toggleProject.bind(this);
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.authenticateGithub = this.authenticateGithub.bind(this);
    }

    componentDidMount(){
        this.state.projectList.forEach((project,index)=>{
            if(project.authInfo.hasAuthenticated){
                if(project.name === 'Github'){
                    this.authenticateGithub(index,project.authInfo);
                }
            }
        });
    }

    open(index,event){
        let clonedProjectState = this.state.projectList.slice();
        clonedProjectState[index].showModal = true;
        this.setState({projectRepos: clonedProjectState})
    }

    close(index,event){
        let clonedProjectState = this.state.projectList.slice();
        clonedProjectState[index].showModal = false;
        this.setState({projectRepos: clonedProjectState})
    }

    toggleProject(index,event){
        let clonedProjectState = this.state.projectList.slice();
        clonedProjectState[index].isActive = !clonedProjectState[index].isActive;
        this.setState({projectRepos: clonedProjectState});
    }

    authenticateGithub(index, authInfo, event) {
        let clonedProjectState = this.state.projectList.slice();
        clonedProjectState[index].auth = new Github({username: authInfo.userName, password: authInfo.password});
        clonedProjectState[index].auth.getUser().listRepos({type: 'public'}).then((response) => {
            clonedProjectState[index].error = null;
            clonedProjectState[index].repoData = response.data.reverse();
            clonedProjectState[index].hasAuthenticated = true;
            clonedProjectState[index].isActive = true;
            clonedProjectState[index].showModal = false;
            this.setState({projectRepos: clonedProjectState});
        }, (err) => {
            clonedProjectState[index].error = err;
            clonedProjectState[index].repoData = [];
            clonedProjectState[index].hasAuthenticated = false;
            clonedProjectState[index].isActive = false;
            this.setState({projectRepos: clonedProjectState});
        });
    }

    render() {
        return (
            <div id="projects">
                {
                    this.state.projectList.map((project,index)=>{
                        switch(project.name){
                            case 'Github':
                                return (
                                    <GithubModal
                                        key={index}
                                        showModal={project.showModal}
                                        close={this.close.bind(this,index)}
                                        authenticate={this.authenticateGithub.bind(this,index)}
                                        error={project.error}
                                        />
                                );
                        }
                    })
                }

                <h1>Projects</h1>
                <ButtonGroup>
                    {
                        this.state.projectList.map((project,index)=>{
                            return (
                                <Button
                                    key={index}
                                    active={project.isActive}
                                    bsStyle={project.isActive?"success":"default"}
                                    onClick={this.open.bind(this,index)}
                                    >
                                    {project.name}
                                </Button>
                            )
                        })
                    }
                </ButtonGroup>
                <hr/>
                <div className="container">
                    {
                        this.state.projectList.map((projectList,index)=>{
                            return (
                                <ProjectList key={index} projects={projectList.repoData} title={projectList.name} />
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Projects;
