import React, {Component, PropTypes} from 'react';
import update from 'immutability-helper';
import { Modal, Button ,Form, FormGroup, Col ,FormControl ,ControlLabel } from 'react-bootstrap';

class GithubModal extends Component{
    constructor(props){
        super(props);
        this.init();
    }

    init(){
        this.state={
            auth:{
                userName: '',
                password: ''
            }
        }
        this.updateUserName = this.updateUserName.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
    }

    updateUserName(event){
        this.setState({
            auth: update(this.state.auth,{
                userName:{
                    $set: event.target.value
                }
            })
        });
    }

    updatePassword(event){
        this.setState({
            auth: update(this.state.auth,{
                password:{
                    $set: event.target.value
                }
            })
        })
    }

    render(){
        return (
            <Modal
                show={this.props.showModal}
                onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Authenticate Github Account
                        {
                            this.props.error !== null? <span style={{color: 'red'}}> Unable to sign In</span> : ''
                        }
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form horizontal>
                        <FormGroup controlId="githubAuth">
                            <Col componentClass={ControlLabel} sm={2}>
                                UserName
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="text"
                                    placeholder="UserName"
                                    onBlur={this.updateUserName}
                                    />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalPassword">
                            <Col componentClass={ControlLabel} sm={2}>
                                Password
                            </Col>
                            <Col sm={10}>
                                <FormControl
                                    type="password"
                                    placeholder="Password"
                                    onBlur={this.updatePassword}
                                    />
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.authenticate.bind(this,this.state.auth)}>Authentiate</Button>
                    <Button onClick={this.props.close}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
GithubModal.propTypes={
    showModal: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    authenticate: PropTypes.func.isRequired
}
export default GithubModal;
