import React, {Component,PropTypes} from 'react';
import {Panel} from 'react-bootstrap';
import Project from './components/Project';


class ProjectList extends Component{
    constructor(props){
        super(props);
        this.init();
        this.sort = this.sort.bind(this);
    }

    init(){
        this.state={
            sortBy: 'Latest',
            sortList: [
                'Latest',
                'Oldest',
                'Ascending',
                'Descending'
            ]
        }
    }

    sort(a,b){
        switch(this.state.sortBy){
            case 'Latest':
                if(a.created_at > b.created_at){
                    return 1;
                }else if(a.created_at < b.created_at){
                    return -1;
                }else{
                    return 0;
                }
            case 'Oldest':
                if(a.created_at < b.created_at){
                    return 1;
                }else if(a.created_at > b.created_at){
                    return -1;
                }else{
                    return 0;
                }
            case 'Ascending':
                break;
            case 'Descending':
                break;
        }
    }

    projectBucket(){
        let bucket = [];
        let tempArr = [];
        let projects = this.props.projects.slice();
        projects.sort(this.sort).forEach((project,index)=>{
            if(!((index + 1) % 3)){
                tempArr.push(project);
                bucket.push(tempArr.slice());
                tempArr = [];
            }else{
                tempArr.push(project);
            }
        });
        if(tempArr.length){
            bucket.push(tempArr)
        }
        console.log(bucket);
        return bucket;
    }

    goToProject(url,event){
        window.open(url, '_blank');
    }

    render(){
        return (
            <div className="projectListContainer">
                {
                    this.props.projects.length !== 0? <h1>{this.props.title} Projects</h1>: ''    
                }
                {
                    this.projectBucket().map((projects,index1)=>{
                        return (
                            <div className="row" key={index1}>
                                {
                                    projects.map((project,index2)=>{
                                        return (
                                            <div className="col-md-4" key={index2}>
                                                <Panel header={project.name} onClick={this.goToProject.bind(this,project.svn_url)}>
                                                    <p>{project.description}</p>
                                                    <p>Main Language: {project.language}</p>
                                                </Panel>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
ProjectList.propTypes={
    projects: PropTypes.array.isRequired,
    title: PropTypes.string.isRequired
}
export default ProjectList;
