import React, { PropTypes } from 'react';
import { Link, History } from 'react-router';

const Tab = ({pos,name,link,active,activateTab}) => {
    return (
        <li role="presentation" className={active ? 'active' : ''} onClick={activateTab.bind(this,link)}>
            <Link to={link}>
                {name}
            </Link>
        </li>
    )
}

Tab.propTypes = {
    pos: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    activateTab: PropTypes.func.isRequired
}

export default Tab;
