import React, {Component , PropTypes} from 'react';
import Tab from './Tab';

class Tabs extends Component {
    constructor(props) {
        super(props);
        
    }

    render() {
        return (
            <ul className="nav nav-tabs" role="tablist">
                {
                    this.props.tabs.map((tab, index)=>{
                        return (
                            <Tab key={index} pos={index} name={tab.name} link={tab.link} active={tab.active} activateTab={this.props.activateTab}/>
                        )
                    })
                }
            </ul>
        )
    }
}

Tabs.propTypes = {
    tabs: PropTypes.array.isRequired,
    activateTab: PropTypes.func.isRequired
}
export default Tabs;
