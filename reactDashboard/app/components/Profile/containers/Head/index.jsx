import React, { Component, PropTypes } from 'react';
import Tabs from './components/Tabs';
require('./index.scss');

class Head extends Component{
    constructor(props){
        super(props);

        this.init();
        this.activateTab = this.activateTab.bind(this);
    }
    init(){
        let tabs =  [
                {
                    name: 'Home',
                    link: '/profile',
                    active: false
                },
                {
                    name: 'FaceBook Feed',
                    link: '/profile/facebook',
                    active: false
                },
                {
                    name: 'Photo Gallery',
                    link: '/profile/photogallery',
                    active: false
                }
                // {
                //     name: 'Twitter Feed',
                //     link: '/profile/twitter',
                //     active: false
                // }
            ];

        this.state = {
            tabs: tabs.map((tab)=>{
                if(tab.link === this.props.activePath){
                    tab.active = true;
                }
                return tab;
            })
        }
    }

    activateTab(link,event){
        this.setState({
            tabs: this.state.tabs.map((tab,index)=>{
                tab.active = link === tab.link;
                return tab;
            })
        })
    }

    render(){
        // Cannot combine css properties
        const backgroundStyle = {
            backgroundSize: 'cover',
            backgroundImage: `url(${this.props.profileBackground})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
        }
        return (
            <div id='ProfileHead'>
                <div className="profileBackground" style={backgroundStyle}>
                    <div className="profilePic">
                        <img src={`${this.props.profilePic}`} className="img-rounded img-responsive" alt="Responsive image" />
                    </div>
                </div>
                <div className="tabContainer">
                    <Tabs tabs={this.state.tabs} activateTab={this.activateTab}/>
                </div>
            </div>
        )
    }
}

Head.propTypes = {
    activePath: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    profession: PropTypes.string.isRequired,
    profilePic: PropTypes.string.isRequired,
    profileBackground: PropTypes.string.isRequired
}

export default Head;
