import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import Firebase from '../../../../config/dbConfig';
require('./index.scss');

var masonryOptions = {
    transitionDuration: 20,
    columnWidth: 200,
    itemSelector: '.grid-item'
};

class PhotoGallery extends Component{
    constructor(props){
        super(props);
        this.init();
    }

    init(){
        this.state = {
            imageList: []
        }
        this.firebaseRef = Firebase.database().ref(`users/${this.props.currentUser.key}/photoBucket`);
    }
    componentWillMount(){
        let updateImageList = (data)=>{
            if(data.val()){
                this.setState({
                    imageList: data.val()
                })
            }
        }
        this.firebaseRef.on('value', updateImageList);
    }

    componentWillUnmount(){
        this.firebaseRef.off();
    }

    render(){
        return (
           <Masonry id="PhotoGallery"
                className={'grid'} // default ''
                elementType={'div'} // default 'div'
                options={masonryOptions} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={true} // default false and works only if disableImagesLoaded is false
            >
                {
                    this.state.imageList.map((element,index)=>{
                       return (
                            <div key={index} className={'grid-item'}>
                                <div className="overlay">
                                    <button className="btn overlayBtn" onClick={this.props.galleryFunc.setProfileImage.bind(this,element)}>Set as Profile Pic</button>
                                    <button className="btn overlayBtn" onClick={this.props.galleryFunc.setBackgroundImage.bind(this,element)}>Set as Background Pic</button>
                                </div>
                                <img className="img-responsive" src={element} />
                            </div>
                        );
                    })
                }
            </Masonry>
        )
    }
}

export default PhotoGallery;
