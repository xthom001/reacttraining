import React, {Component, PropTypes} from 'react';
import { Modal, Button ,Form, FormGroup, Col ,FormControl ,ControlLabel } from 'react-bootstrap';

class UserInfoModal extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const personal = this.props.personal;
        return (
            <Modal
                show={this.props.showModal}
                onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        Update Profile
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form horizontal>
                        {
                            Object.keys(personal).map((props,index)=>{
                                if(props === 'media'){
                                    return (
                                        Object.keys(personal[props]).filter((propName)=> { return propName.indexOf('_src') === -1; }).map((imageName,pos)=>{
                                            return (
                                                <div>
                                                    <FormGroup key={`${pos}media`} controlId='formControlsFile'>
                                                        <Col componentClass={ControlLabel} sm={4}>
                                                            {imageName}
                                                        </Col>
                                                        <Col sm={8}>
                                                            <FormControl type='file' ref={`${imageName}_src`} name={imageName} defaultValue={personal[props][imageName]} onChange={this.props.updateTempPersonalMedia}/>
                                                        </Col>
                                                    </FormGroup>

                                                </div>
                                            )
                                        })
                                    )
                                }else{
                                    return (
                                        <FormGroup key={index} controlId={this.props.determineFormIdType(props)}>
                                            <Col componentClass={ControlLabel} sm={2}>
                                                {props}
                                            </Col>
                                            <Col sm={10}>
                                                <FormControl name={props} type={this.props.determineFormControlType(props)} defaultValue={personal[props]} onBlur={this.props.updateTempPersonal} />
                                            </Col>
                                        </FormGroup>
                                    )
                                }
                            })
                        }
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.update}>Update</Button>
                    <Button onClick={this.props.close}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}
UserInfoModal.propTypes= {
    personal: PropTypes.object.isRequired,
    showModal: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    updateTempPersonalMedia: PropTypes.func.isRequired,
    updateTempPersonal: PropTypes.func.isRequired,
    determineFormIdType: PropTypes.func.isRequired,
    determineFormControlType: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired
}
export default UserInfoModal;
