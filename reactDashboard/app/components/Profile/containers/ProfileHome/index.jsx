import React, { Component } from 'react';
import update from 'immutability-helper';
import UserInfoModal from './components/UserInfoModal';
require('./index.scss');

class ProfileHome extends Component{
    constructor(props){
        super(props);
        this.init();
    }

    init(){
        this.state = {
            showModal: false,
            modalUserData: update({},{ $merge: this.props.currentUser})
        }
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.update = this.update.bind(this);
        this.updateTempPersonal = this.updateTempPersonal.bind(this);
        this.updateTempPersonalMedia = this.updateTempPersonalMedia.bind(this);
    }

    close(){
        this.setState({
            showModal: false
        });
    }

    update(){
        this.setState({
            showModal: false
        });
        this.props.updateUser(this.state.modalUserData);
    }

    open(){
        this.setState({
            showModal: true
        })
    }

    updateTempPersonalMedia(event){
        const file = event.target.files[0];
        const propName = event.target.name;
        this.setState({
            modalUserData: update(this.state.modalUserData,{
                personal:{
                    media: {
                        [propName]: {
                            $set: this.trimImagePath(event.target.value)
                        },
                        [`${propName}_src`]: {
                            $set: file
                        }
                    }
                }
            })
        });
    }

    updateTempPersonal(event){
        this.setState({
            modalUserData:  update(this.state.modalUserData,{
                    personal:{
                        [event.target.name]: {
                            $set: event.target.value
                        }
                    }
                })
        })
    }

    trimImagePath(str){
        let strArr = str.split('\\');
        return strArr[strArr.length -1];
    }

    determineFormIdType(props){
        switch(props){
            case 'email':
                return 'formHorizontalEmail';
            case 'media':
                return 'formControlsFile';
            default:
                return 'formControlsText'
        }
    }

    determineFormControlType(props){
        switch(props){
            case 'email':
                return 'email';
            case 'media':
                return 'file';
            default:
                return 'text';
        }
    }

    render(){
        const currentUserPersonal = this.props.currentUser.personal;
        return (
            <div id="home">
                <UserInfoModal
                    personal={this.state.modalUserData.personal}
                    showModal={this.state.showModal}
                    close={this.close}
                    updateTempPersonalMedia={this.updateTempPersonalMedia}
                    updateTempPersonal={this.updateTempPersonal}
                    determineFormIdType={this.determineFormIdType}
                    determineFormControlType={this.determineFormControlType}
                    update={this.update}
                    />
                <div className="panel panel-default">
                  <div className="panel-body">
                      {
                          Object.keys(currentUserPersonal).filter((prop) => prop !== 'media').map((prop,index)=>{
                              return (
                                  <div key={index} className="well">{currentUserPersonal[prop]}</div>
                              )
                          })
                      }
                  </div>
                </div>
                <i className="glyphicon glyphicon-cog" onClick={this.open}></i>
            </div>
        )
    }
}

export default ProfileHome;
