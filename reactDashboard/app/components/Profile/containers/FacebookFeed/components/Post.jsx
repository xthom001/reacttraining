import React, {Component, PropTypes} from 'react';
require('./Post.scss');

class Post extends Component {
    constructor(props) {
        super(props);

        var post = this.props.post;
        var hasAttachments = post.attachment? true: false;

        this.state={
            hasAttachments ,
            hasStory: post.story? true: false ,
            hasMessage: post.message? true: false,
            hasMedia: this.hasMedia(post.attachment, hasAttachments)
        }
    }

    hasMedia(attachment,hasAttachments){
        if(attachment && hasAttachments){
            return attachment[0].type !== 'unavailable' && attachment[0].media !== undefined;
        }
        return false;
    }

    render() {
        return (
            <li className="list-group-item">
                <div className="panel-body" >
                    {this.props.post.message &&
                        <div className="messageCont">
                            <h4>
                                {this.props.post.message.indexOf('http') !== -1 &&
                                    <a href={this.props.post.message} target="_blank">
                                        {this.props.post.message}
                                    </a>
                                }
                                {this.props.post.message.indexOf('http') === -1 &&
                                    <span>
                                        {this.props.post.message}
                                    </span>
                                }
                            </h4>
                        </div>
                    }
                    {this.props.post.story &&
                        <div className="StoryCont">
                            <h4>
                                {this.props.post.story &&
                                    <span>
                                        {this.props.post.story}
                                    </span>
                                }
                            </h4>
                        </div>
                    }
                    {this.state.hasAttachments && this.state.hasMedia &&
                        <div className="imageContainer">
                            <img src={this.props.post.attachment[0].media.image.src} alt="img" className="img-thumbnail"/>
                        </div>
                    }
                    {this.state.hasAttachments && this.props.post.attachment[0].description &&
                        <div className="descriptionCont">
                            <h4>
                                {this.props.post.attachment[0].description}
                            </h4>
                        </div>
                    }
                 </div>
            </li>
        )
    }
}

Post.propTypes = {
    post: PropTypes.object.isRequired
}

export default Post;
