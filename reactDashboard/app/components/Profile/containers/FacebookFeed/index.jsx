import React, {Component} from 'react';
import Loader from '../../../Common/Loader';
import util from '../../../../utils';
import Post from './components/Post';
require('./index.scss');

class FaceBookFeed extends Component {
    constructor(props) {
        super(props);
        this.init();
    }

    init(){
        this.state = {
            posts: []
        }
        this.cancelablePromise = null;
    }

    componentDidMount() {
        this.mounted = true;
        FB.getLoginStatus((response) => {
                if (response.status === 'connected') {
                    console.log('Logged in.');
                    this.getUsersFeed(response.authResponse.accessToken);
                } else {
                    FB.login((response) => {
                        console.log(response)
                    }, {scope: 'user_posts'});
                    this.getUsersFeed(response.authResponse.accessToken);
                }
            });
    }


    getUsersFeed(accessToken) {
        var map = new Map();
        if(this.mounted){
            FB.api('/me/feed', 'GET', {
                access_token: accessToken
            }, (feed) => {
                if (feed && !feed.error) {
                    // Gets the inital post feed
                    var containerArr = feed.data;
                    // Store those feed in a map with the key being the id
                    containerArr.forEach((container)=>{
                        map.set(container.id,container);
                    });
                    // get the additional property associated with the feed
                    FB.api('/me/feed?fields=attachments', 'GET', {
                        access_token: accessToken
                    },(attachments)=>{
                        var newArr = [];
                        var attachmentArr = attachments.data;

                        /*
                            Loops through the attachments and then
                            map that attachment with the corresponding feed
                        */
                        attachmentArr.forEach((attachment)=>{
                            var container = map.get(attachment.id);
                            if(attachment.hasOwnProperty('attachments')){
                                container.attachment = attachment.attachments.data;
                            }else {
                                container.attachment = null;
                            }
                            newArr.push(container);
                        });
                        if(this.mounted){
                            this.setState({
                                posts: newArr
                            });
                        }
                    })
                }
            });
        }
    }

    componentWillUnmount(){
        this.mounted = false;
    }

    render() {
        return this.state.posts.length
        ?   <ul className="list-group">
                {
                    this.state.posts.map((post,index)=>{
                        return (
                            <Post key={index} post={post}/>
                        )
                    })
                }
            </ul>
        :   <div className='spinnerContainer'>
                <Loader />
            </div>
    }
}

export default FaceBookFeed;
