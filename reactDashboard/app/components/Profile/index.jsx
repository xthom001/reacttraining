import React, {Component } from 'react';
import Firebase from '../../config/dbConfig';
import update from 'immutability-helper';
import Head from './containers/Head';

var userProfile = {
    personal: {
        firstName: 'Xavier',
        lastName: 'Thomas',
        age: 24,
        birthday: '02/11/1992',
        address: '2520 Maroon bells Ave',
        state: 'Colorado',
        city: 'Colorado Springs',
        email: 'xthom001@outlook.com',
        phone: '9548828795',
        media: {
            profilePic: 'mybrotherAndI.png',
            profileBackground: 'fox_stone.jpg'
        },
        profession: 'Software Developer'
    },
    likes: {
        food: [
            'Akee & Saltfish',
            'Fried Dumplins',
            'Brown Stew Chicken',
            'Pizza',
            'Baked Mac & Cheese'
        ],
        movies: [
            'Interstellar',
            'Ip man 3',
            'King Fu Panda 3',
            'Avengers',
            'Dead Pool'
        ],
        places: [
            {
                name: 'DC',
                visited: true
            },
            {
                name: 'Jamaica',
                visited: true
            },
            {
                name: 'France',
                visited: false
            },
            {
                name: 'Italy',
                visited: false
            },
            {
                name: 'Seattle ',
                visited: false
            }
        ],
        music: [
            {
                genre: 'Rap',
                artist: 'Lil Wayne',
                songs: []
            }
        ],
        books: [
            {
                author: 'Brandon Sanderson',
                books: [
                    'Hero Of Ages'
                ]
            }
        ]
    }
}

class Profile extends Component{
    constructor(props){
        super(props);
        this.init();
    }

    init(){
        this.state = {
            activePath: '',
        }
    }

    componentWillMount(){
        this.setState({
            activePath: this.props.location.pathname
        });
    }

    render(){
        const personalInfo = this.props.currentUser.personal;
        return (
            <div id="profile">
                <Head
                    activePath={this.state.activePath}
                    name={`${personalInfo.firstName} ${personalInfo.lastName}`}
                    profession={personalInfo.profession}
                    profilePic={personalInfo.media.profilePic}
                    profileBackground={personalInfo.media.profileBackground}
                    />
                <div id="profileBody">
                    {this.props.children && React.cloneElement(this.props.children, {
                        currentUser: this.props.currentUser,
                        updateUser: this.props.updateUser,
                        galleryFunc: this.props.galleryFunc
                    })}
                </div>
            </div>

        )
    }
}

export default Profile;
