import React, { Component, PropTypes } from 'react';
require('./SideButton.scss');

class SideButton extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div id='sideButton'>
              <aside className='arrow' onClick={this.props.toggleSideMenu}>Menu</aside>
            </div>
        )
    }
}

SideButton.propTypes ={
    toggleSideMenu: PropTypes.func.isRequired
}

export default SideButton;
