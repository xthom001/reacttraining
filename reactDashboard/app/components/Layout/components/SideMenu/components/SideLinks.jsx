import React, { PropTypes} from 'react';
import { Link } from 'react-router'
require('./SideLinks.scss');

const SideList = ({name,link,mainClass,activateLink}) => {
    return (
        <li className={`links ${mainClass}`}>
            <Link to={link} activeStyle={{ color: 'white' }} onClick={activateLink.bind(this,link)}>
                {name}
            </Link>
        </li>
    );
};

SideList.propTypes = {
    mainClass: PropTypes.string,
    name: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    activateLink: PropTypes.func.isRequired
};

export default SideList;
