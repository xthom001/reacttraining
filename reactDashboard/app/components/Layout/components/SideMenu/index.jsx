import React, { Component, PropTypes } from 'react';
import SideLinks from './components/SideLinks';

class SideMenu extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <ul className="sidebar-nav">
                {
                    this.props.links.map((location,index) =>{
                        return (
                            <SideLinks
                                key={index}
                                mainClass={location.active?'sidebar-brand': ''}
                                name={location.name}
                                link={location.link}
                                activateLink={this.props.activateLink}
                                />
                        )
                    })
                }
                <li className='links'>
                    <a onClick={this.props.logout}>
                        Log out
                    </a>
                </li>
            </ul>
        );
    }

}

SideMenu.propTypes = {
    links: PropTypes.array.isRequired,
    logout: PropTypes.func.isRequired,
    activateLink: PropTypes.func.isRequired
}

export default SideMenu;
