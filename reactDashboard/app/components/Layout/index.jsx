import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import update from 'immutability-helper';
import './layout.scss';
import SideMenu from './components/SideMenu';
import SideButton from '../Common/SideButton';
import Firebase from '../../config/dbConfig';

class Layout extends Component {
    constructor(props){
        super(props);
        this.init();
    }

    init(){
        this.links = [
                {
                    name: 'React Dash',
                    link: '/',
                    active: true
                },
                {
                    name: 'Todo',
                    link: '/todo',
                    active: false
                },
                {
                    name: 'Profile',
                    link: '/profile',
                    active: false
                },
                {
                    name: 'Projects',
                    link: '/projects',
                    active: false
                }
            ];
        this.state = {
            authenticated: false,
            user: {},
            toggled: true,
            links: this.links
        };
        this.toggleSideMenu = this.toggleSideMenu.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
        this.signUp = this.signUp.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.activateLink = this.activateLink.bind(this);
        this.setProfileImage = this.setProfileImage.bind(this);
        this.setBackgroundImage = this.setBackgroundImage.bind(this);
        this.updateUserProjectAuth = this.updateUserProjectAuth.bind(this);
        this.firebaseAuth = Firebase.auth();
        this.firebaseDatabaseRef = null;
        this.firebaseStorageRef = null;
    }

    componentWillMount(){
        this.firebaseAuth.onAuthStateChanged(user =>{
            if(user){

                this.firebaseDatabaseRef = Firebase.database().ref(`users/${user.uid}`);
                this.firebaseStorageRef = Firebase.storage().ref(`${user.uid}/images/`);

                this.firebaseDatabaseRef.once('value', (currentUser)=>{
                    this.setState({
                        links: this.state.links.map((path)=>{
                            path.active = path.link === '/profile';
                            return path;
                        }),
                        authenticated: true,
                        toggled: false,
                        user: currentUser.val()
                    });
                    if(this.props.location.pathname === '/'){
                        hashHistory.push('/profile');
                    }else{
                        hashHistory.push(`${this.props.location.pathname}`);
                    }
                });
                this.firebaseDatabaseRef.on('value', (currentUser)=>{
                    this.setState({
                        user: currentUser.val()
                    });
                });
            }else{
                this.setState({
                    authenticated: false
                });
            }
        });
    }

    updateUser(updatedUserObj){
        /*
            Sets the photoBucket property on the user object because
            Firebase stores empty objects and empty arrays as null
        */
        if(!updatedUserObj.hasOwnProperty('photoBucket')){
            updatedUserObj['photoBucket'] = [];
        }

        let checkIfUpdatingImg = (userObj) =>{
            return this.state.user.personal.media.profileBackground.indexOf(userObj.personal.media.profileBackground) === -1
                && this.state.user.personal.media.profilePic.indexOf(userObj.personal.media.profilePic) === -1;
        }

        let checkIfUpdatingProfileImg = (userObj,stateUser) =>{
            return this.state.user.personal.media.profilePic.indexOf(userObj.personal.media.profilePic) === -1;
        }

        let checkIfUpdatingProfileBackground = (userObj,stateUser) =>{
            return this.state.user.personal.media.profileBackground.indexOf(userObj.personal.media.profileBackground) === -1;
        }

        let storeProfilePic = (pic)=> {
            return this.firebaseStorageRef.child(`${pic.personal.media.profilePic}`).put(pic.personal.media.profilePic_src)
        }

        let storeProfileBackground = (pic)=>{
            return this.firebaseStorageRef.child(`${pic.personal.media.profileBackground}`).put(pic.personal.media.profileBackground_src)
        }

        let updateUserState = (updateObj)=>{
            delete updateObj.personal.media.profileBackground_src;
            delete updateObj.personal.media.profilePic_src;
            this.firebaseDatabaseRef.set(updateObj);
        }

        if(updatedUserObj.personal.media.profileBackground && updatedUserObj.personal.media.profilePic && checkIfUpdatingImg(updatedUserObj)){
            Firebase.Promise.all([storeProfilePic(updatedUserObj),storeProfileBackground(updatedUserObj)]).then((response)=>{
                updatedUserObj.personal.media.profilePic = response[0].downloadURL;
                updatedUserObj.personal.media.profileBackground = response[1].downloadURL;
                updatedUserObj['photoBucket'] = updatedUserObj['photoBucket'].concat(response.map((img)=> {return img.downloadURL}));
                updateUserState(updatedUserObj);
            });
        }else if(updatedUserObj.personal.media.profileBackground && checkIfUpdatingProfileBackground(updatedUserObj)){
            storeProfileBackground(updatedUserObj).then((response)=>{
                updatedUserObj.personal.media.profileBackground = response.downloadURL;
                 updatedUserObj['photoBucket'].push(response.downloadURL);
                updateUserState(updatedUserObj);
            })
        }else if(updatedUserObj.personal.media.profilePic && checkIfUpdatingProfileImg(updatedUserObj)){
            storeProfilePic(updatedUserObj).then((response)=>{
                updatedUserObj.personal.media.profilePic = response.downloadURL;
                updatedUserObj['photoBucket'].push(response.downloadURL);
                updateUserState(updatedUserObj);
            });
        }else{
            updateUserState(updatedUserObj);
        }
    }

    setProfileImage(img,event){
        var updateUserObj = update(this.state.user,{
            personal: {
                media: {
                    profilePic: {
                        $set: img
                    }
                }
            }
        });
        this.firebaseDatabaseRef.set(updateUserObj)
    }

    updateUserProjectAuth(userObj,authInfo){
        userObj.projectList.push(authInfo);
        this.firebaseDatabaseRef.set(userObj);
    }

    setBackgroundImage(img,event){
        var updateUserObj = update(this.state.user,{
            personal: {
                media: {
                    profileBackground: {
                        $set: img
                    }
                }
            }
        });
        this.firebaseDatabaseRef.set(updateUserObj)
    }

    toggleSideMenu(){
        this.setState({
            toggled: !this.state.toggled
        });
    }

    login(userCred){
        this.firebaseAuth.signInWithEmailAndPassword(userCred.email,userCred.password).catch((err)=>{
            console.log(err);
        })
    }

    logout(event){
        event.preventDefault();
        this.firebaseAuth.signOut();
        this.setState({
            authenticated: false,
            toggled: true,
            user: {},
            Links: this.links
        })
        hashHistory.push('/');
    }

    signUp(userCred){
        console.log(userCred);
        this.firebaseAuth.createUserWithEmailAndPassword(userCred.email,userCred.password).then((user)=>{
            let currentUser = {
                photoBucket: [],
                key: user.uid,
                personal: {
                    firstName: '',
                    lastName: '',
                    age: 0,
                    birthday: '',
                    address: '',
                    state: '',
                    city: '',
                    email: user.email,
                    phone: '',
                    media: {
                        profilePic: '',
                        profileBackground: ''
                    },
                    profession: ''
                },
                likes: {
                    food: [],
                    movies: [],
                    places: [],
                    music: [
                        {
                            genre: '',
                            artist: '',
                            songs: []
                        }
                    ],
                    books: []
                }
            }
            Firebase.database().ref(`users/${user.uid}`).set(currentUser).then((response)=>{
                console.log(response,currentUser);
               this.setState({
                    user: currentUser,
                    authenticated: true,
                    toggled: false
                })
            });
        },(err)=>{
            console.log(err);
        })
    }

    componentWillUnmount(){
        this.firebaseStorageRef.off();
        this.firebaseDatabaseRef.off();
    }

    activateLink(activatedLink,event){
        this.setState({
            links: this.state.links.map((path)=>{
                path.active = path.link === activatedLink;
                return path;
            })
        })
    }

    render(){
        return(
            <div id="wrapper" className={this.state.toggled? 'toggled': ''}>
                <div id="sidebar-wrapper">
                    <SideMenu links={this.state.links} logout={this.logout} activateLink={this.activateLink} />
                </div>
                {this.state.authenticated
                    ? <SideButton toggleSideMenu={this.toggleSideMenu} />
                    : ''
                }

                <div id="page-content-wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-12">
                                {this.props.children && React.cloneElement(this.props.children, {
                                    login: this.login,
                                    signUp: this.signUp,
                                    currentUser: this.state.user,
                                    updateUser: this.updateUser,
                                    galleryFunc: {setProfileImage: this.setProfileImage,setBackgroundImage: this.setBackgroundImage},
                                    updateUserProjectAuth : this.updateUserProjectAuth
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Layout;
