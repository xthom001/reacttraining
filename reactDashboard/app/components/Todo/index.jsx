import React, {Component} from 'react';
import TodoList from './components/TodoList';
import Firebase from '../../config/dbConfig';
import update from 'immutability-helper';
import utils from '../../utils';
import Loader from '../Common/Loader';

require('./Todo.scss');

class Todo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todo: {
                items: [],
                value: ''
            },
            user: this.props.currentUser
        }
        this.firebaseRef = Firebase.database().ref(`${this.props.route.path}/${this.props.currentUser.key}`);

        this.handleChange = this.handleChange.bind(this);
        this.addTodo = this.addTodo.bind(this);
        this.toggleCompleted = this.toggleCompleted.bind(this);
        this.removeTodo = this.removeTodo.bind(this);
    }

    componentWillMount() {
        let UpdateData = (data)=>{
            if(data.val()){
                this.setState({
                    todo:   update(this.state.todo,{
                                items:{
                                        $set: data.val()
                                    }
                                }
                            ),
                    value: ''
                })
            }
        }
        this.firebaseRef.on('value', UpdateData);
    }

    // Handles changes in the input
    handleChange(event) {
        this.setState({
            todo: {
                items: this.state.todo.items,
                value: event.target.value
            }
        });
    }

    addTodo(e) {
        e.preventDefault();
        e.stopPropagation();
        var itemsArr = this.state.todo.items.slice();

        var newTodo = {
            text: this.state.todo.value,
            key: utils.hashCode(`${this.state.todo.items.length}${new Date()}`),
            date: new Date().toISOString(),
            completed: false
        };
        itemsArr.push(newTodo);
        this.firebaseRef.set(itemsArr);
        this.refs.Item.value = '';

    }

    removeTodo(key) {
         var itemsArr = this.state.todo.items.filter((item) => {
             return item.key !== key;
         });

        this.firebaseRef.set(itemsArr,(err) =>{
            if(err){
                console.log(err);
            }else{
                this.setState({
                    todo:{
                        items: itemsArr,
                        value: ''
                    }
                })
            }
        });
    }

    toggleCompleted(key) {
        var itemsArr = this.state.todo.items.map((item) => {
            if (item.key === key) {
                item.completed = !item.completed;
            }
            return item;
        });

        this.firebaseRef.set(itemsArr);
    }

    render() {
        return (
            <div id="todoContainer" className="panel panel-default">
                <div className="panel-heading">
                    <h1>Current Tasks</h1>
                </div>
                {this.state.todo.items.length
                    ?
                        <div className="panel-body">
                            <form className="form-inline">
                                <div className="form-group">
                                    <label htmlFor="exampleInputEmail1">Add Todo:</label>
                                    <input type="text" className="form-control" ref='Item' placeholder='Add Item' onBlur={this.handleChange}/>
                                </div>
                                <button type="submit" className="btn btn-default" onClick={this.addTodo}>Submit</button>
                            </form>
                            <TodoList items={this.state.todo.items} toggleCompleted={this.toggleCompleted} removeTodo={this.removeTodo}/>
                        </div>
                    :
                        <div className='spinnerContainer'>
                            <Loader />
                        </div>
                }
            </div>
        )
    }
}
export default Todo;
