import React, { Component, PropTypes } from 'react';
import utils from '../../../utils';

class TodoItem extends Component{
    constructor(props){
        super(props);
        this.toggleCompleted = this.toggleCompleted.bind(this);
        this.removeTodo = this.removeTodo.bind(this);
    }

    toggleCompleted(){
        this.props.toggleCompleted(this.props.identifier);
    }

    removeTodo(){
        this.props.removeTodo(this.props.identifier);
    }

    render(){
        return (
            <li  className='item list-group-item' style={{ backgroundColor: this.props.isCompleted ? 'rgb(255, 58, 58)': 'white' }}>
                <div className="row">
                    <div className="col-sm-8">
                        <p className='content'>{this.props.text}</p>
                        <p className='time'>{utils.formatTime(this.props.date)}</p>
                    </div>
                    <div className="col-sm-2">
                        <p>Complete</p>
                        <div className="checkbox">
                            <label>
                                <input type="checkbox" defaultChecked={this.props.isCompleted} onClick={this.toggleCompleted}/>
                            </label>
                        </div>
                    </div>
                    <div className="col-sm-2">
                        <button className="btn-danger" onClick={this.removeTodo}>Delete</button>
                    </div>
                </div>
            </li>
        )
    }
}


TodoItem.propTypes = {
    identifier: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    isCompleted: PropTypes.bool.isRequired,
    date: PropTypes.any.isRequired,
    toggleCompleted: PropTypes.func.isRequired,
    removeTodo: PropTypes.func.isRequired
}

export default TodoItem;
