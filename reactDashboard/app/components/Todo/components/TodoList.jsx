import React, { Component, PropTypes } from 'react';
import TodoItem from './TodoItem';

class TodoList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
         <ul className='list-group'>
            {
                this.props.items.map((item,index) =>{
                    return (
                        <TodoItem
                            key={index}
                            identifier={item.key}
                            text={item.text}
                            isCompleted={item.completed}
                            date={item.date}
                            toggleCompleted={this.props.toggleCompleted}
                            removeTodo={this.props.removeTodo}
                            />
                    )
                }, this)
            }
         </ul>
        )
    }
}

TodoList.propTypes = {
    items: PropTypes.array.isRequired,
    toggleCompleted: PropTypes.func.isRequired,
    removeTodo: PropTypes.func.isRequired
}

export default TodoList;
