import React, {Component} from 'react';
require('./index.scss');

class MainScreen extends Component {
    constructor(props) {
        super(props);
        this.state ={
            email: '',
            password: ''
        }
        this.login = this.login.bind(this);
        this.signUp = this.signUp.bind(this);
        this.setUserEmail = this.setUserEmail.bind(this);
        this.setUserPass = this.setUserPass.bind(this);
    }

    login(event){
        event.preventDefault();
        this.props.login(this.state);
    }
    
    signUp(event){
        event.preventDefault();
        this.props.signUp(this.state);
    }

    setUserEmail(event){
        this.setState({
            email: event.target.value
        });
    }

    setUserPass(event){
        this.setState({
            password: event.target.value
        });
    }

    render() {
        return (
            <div className="wrapper">
                <form className="form-signin">
                    <h2 className="form-signin-heading">Please login</h2>
                    <input type="text" className="form-control" name="username" placeholder="Email Address" autoFocus="" onBlur={this.setUserEmail}/>
                    <input type="password" className="form-control" name="password" placeholder="Password" onBlur={this.setUserPass} />
                    <label className="checkbox">
                        <input type="checkbox" />
                        Remember me
                    </label>
                    <button className="btn btn-lg btn-primary btn-block"  onClick={this.login}>Login</button>
                    <button className="btn btn-lg btn-primary btn-block"  onClick={this.signUp}>SignUp</button>
                </form>
            </div>
        )
    }
}

export default MainScreen;
