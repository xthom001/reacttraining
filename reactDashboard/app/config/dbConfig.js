import Firebase from 'firebase';
import {FIREBASEAPI} from './externalApi';

// Initialize Firebase
Firebase.initializeApp({
    apiKey: FIREBASEAPI.apiKey,
    authDomain: FIREBASEAPI.authDomain,
    databaseURL: FIREBASEAPI.databaseURL,
    storageBucket: FIREBASEAPI.storageBucket,
    messagingSenderId: FIREBASEAPI.messagingSenderId
});

export default Firebase;
