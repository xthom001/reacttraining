import React from 'react';
import { hashHistory, Router, Route, IndexRoute, IndexRedirect} from 'react-router';
import {FBAPI, TWITTERAPI} from './externalApi';
import Firebase from './dbConfig';
import Layout from '../components/Layout';
import MainScreen from '../components/MainScreen';
import Todo from '../components/Todo';
import Profile from '../components/Profile';
import {ProfileHome,FaceBook,Twitter, PhotoGallery} from '../components/Profile/containers';
import Projects from '../components/Projects';

/*
    Initalizes FB js sdk then grants access to the page
    If sdk is alread initalized then grants access to the page
*/
const facebookInitialization = (callback)=>{
    window.fbAsyncInit = () => {
            FB.init({appId: FBAPI.appId, xfbml: false, version: 'v2.8'});
            FB.AppEvents.logPageView();
            window.FBinitalized = true;
            callback();
        };
        if(window.FBinitalized){
            callback();
        }
        (function(d, s, id) {
            var js,
                fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
}

const executeRoute = (nextState,replace,callback)=>{
    var user = Firebase.auth().currentUser;
    if(user){
        console.log(nextState.location.pathname);
        switch(nextState.location.pathname){
            case '/profile/facebook':
                facebookInitialization(callback);
                break;
            case '/todo':
            case '/profile':
            case '/projects':
                callback();
                break;
        }
    }else{
        replace('/');
        callback();
    }
}

/*
    TODO Find a new api to use since twitter and tumblr api are being ridciulous
*/
const OtherAPIInitialization = (nextState, replaceState, callback)=>{
    callback();
}

export default (
    <Router history={hashHistory}>
        <Route path='/' component={Layout}>
            <IndexRoute component={MainScreen} />
            <Route path='todo' component={Todo} onEnter={executeRoute}></Route>
            <Route path='profile' component={Profile} onEnter={executeRoute}>
                <IndexRoute component={ProfileHome} />
                <Route path='facebook' component={FaceBook} onEnter={executeRoute}></Route>
                <Route path='photogallery' component={PhotoGallery}></Route>
                <Route path='twitter' component={Twitter} ></Route>
            </Route>
            <Route path="projects" component={Projects} onEnter={executeRoute}></Route>
        </Route>
        <Route path="*">
            <IndexRedirect to="/" />
        </Route>
    </Router>
);

// <IndexRedirect to="home" />
// <Route path='home' component={}></Route>
